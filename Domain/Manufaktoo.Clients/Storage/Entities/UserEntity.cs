﻿using Domain.Entities.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;

namespace Manufaktoo.Users.Storage.Entities
{
    public class UserEntity : IdentityUser<Guid>, IEntity
    {
        public DateTime BirthDate { get; set; }
    }
}
