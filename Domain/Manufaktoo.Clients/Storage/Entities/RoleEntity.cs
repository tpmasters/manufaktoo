﻿using Domain.Entities.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;

namespace Manufaktoo.Users.Storage.Entities
{
    public class RoleEntity : IdentityRole<Guid>, IEntity
    {
    }
}
