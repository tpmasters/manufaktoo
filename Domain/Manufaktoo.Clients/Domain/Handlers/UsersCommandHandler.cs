﻿using Domain.Commands;
using Domain.Events;
using Manufaktoo.Users.Contracts.Commands;
using Manufaktoo.Users.Contracts.Events;
using Manufaktoo.Users.Storage;
using Manufaktoo.Users.Storage.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Manufaktoo.Users.Domain.Handlers
{
    public class UsersCommandHandler :
        IAsyncCommandHandler<CreateUserCommand>,
        IAsyncCommandHandler<UpdateUserCommand>,
        IAsyncCommandHandler<DeleteUserCommand>
    {
        private readonly UsersDbContext dbContext;
        private readonly IEventBus eventBus;

        private DbSet<UserEntity> Users;

        public UsersCommandHandler(UsersDbContext UsersDbContext, IEventBus eventBus)
        {
            this.dbContext = UsersDbContext;
            Users = dbContext.Users;
            this.eventBus = eventBus;
        }

        public async Task Handle(CreateUserCommand command)
        {
            Users.Add(new UserEntity()
            {
                UserName = command.Name,
                Email = command.Email,
                BirthDate = command.BirthDate
            });

            await dbContext.SaveChangesAsync();
        }

        public async Task Handle(UpdateUserCommand command)
        {
            var client = await Users.FindAsync(command.Id);
            client.UserName = command.Name;
            client.Email = command.Email;
            dbContext.Update(client);
            await dbContext.SaveChangesAsync();
        }

        public async Task Handle(DeleteUserCommand command)
        {
            var client = await Users.FindAsync(command.Id);
            dbContext.Remove(client);
            await dbContext.SaveChangesAsync();
            await eventBus.Publish(new UserDeleted(command.Id));
        }
    }
}
