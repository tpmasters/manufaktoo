﻿using Domain.Queries;
using Manufaktoo.Users.Contracts.Queries;
using Manufaktoo.Users.Storage;
using Manufaktoo.Users.Storage.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manufaktoo.Users.Views
{
    public class UsersQueryHandler :
        IAsyncQueryHandler<GetUserList, List<UserListItem>>,
        IAsyncQueryHandler<GetUser, UserItem>
    {
        private IQueryable<UserEntity> Users;

        public UsersQueryHandler(UsersDbContext dbContext)
        {
            Users = dbContext.Users;
        }

        public Task<List<UserListItem>> Handle(GetUserList query)
        {
            return Users
                .Select(client => new UserListItem
                {
                    Id = client.Id,
                    Name = client.UserName
                })
                .ToListAsync();
        }

        public Task<UserItem> Handle(GetUser query)
        {
            return Users
                .Select(client => new UserItem
                {
                    Id = client.Id,
                    Name = client.UserName,
                    Email = client.Email
                })
                .SingleOrDefaultAsync(client => client.Id == query.Id);
        }
    }
}
