﻿using Manufaktoo.Users.Contracts.DTOs;

namespace Manufaktoo.Users.Contracts.Validators
{
    public class RegisterNewClientCommandValidation : ClientValidation<CreateUser>
    {
        public RegisterNewClientCommandValidation()
        {
            ValidateName();
            ValidateBirthDate();
            ValidateEmail();
        }
    }
}
