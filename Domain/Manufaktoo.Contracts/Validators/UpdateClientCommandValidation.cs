﻿using FluentValidation;
using Manufaktoo.Users.Contracts.DTOs;
using System;

namespace Manufaktoo.Users.Contracts.Validators
{
    public class UpdateClientCommandValidation : ClientValidation<UpdateUser>
    {
        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }

        public UpdateClientCommandValidation()
        {
            ValidateId();
            ValidateName();
            ValidateBirthDate();
            ValidateEmail();
        }
    }
}
