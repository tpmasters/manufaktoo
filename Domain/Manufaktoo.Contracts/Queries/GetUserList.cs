﻿using Domain.Queries;
using System;
using System.Collections.Generic;

namespace Manufaktoo.Users.Contracts.Queries
{
    public class UserListItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class GetUserList : IQuery<List<UserListItem>>
    {
    }
}
