﻿using Domain.Queries;
using System;

namespace Manufaktoo.Users.Contracts.Queries
{
    public class UserItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class GetUser : IQuery<UserItem>
    {
        public Guid Id { get; }

        public GetUser(Guid id)
        {
            Id = id;
        }

    }
}
