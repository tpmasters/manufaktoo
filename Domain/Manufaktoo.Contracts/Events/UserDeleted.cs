﻿using Domain.Events;
using System;

namespace Manufaktoo.Users.Contracts.Events
{
    public class UserDeleted : IEvent
    {
        public Guid Id { get; }

        public UserDeleted(Guid id)
        {
            Id = id;
        }
    }
}
