﻿using Domain.Events;
using Manufaktoo.Users.Contracts.DTOs;
using System;

namespace Manufaktoo.Users.Contracts.Events
{
    public class UserCreated : IEvent
    {
        public Guid Id { get; }
        public CreateUser Data { get; }

        public UserCreated(Guid id, CreateUser data)
        {
            Id = id;
            Data = data;
        }
    }
}
