﻿using Domain.Events;
using Manufaktoo.Users.Contracts.DTOs;
using System;

namespace Manufaktoo.Users.Contracts.Events
{
    public class UserUpdated : IEvent
    {
        public Guid Id { get; }
        public CreateUser Data { get; }

        public UserUpdated(Guid id, CreateUser data)
        {
            Id = id;
            Data = data;
        }
    }
}
