﻿using Domain.Commands;
using Manufaktoo.Users.Contracts.DTOs;
using System;

namespace Manufaktoo.Users.Contracts.Commands
{
    public class CreateUserCommand : ICommand
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }

        public CreateUserCommand(string name, string email, DateTime birthDate)
        {
            Name = name;
            Email = email;
            BirthDate = birthDate;
        }

        public CreateUserCommand(CreateUser createClient)
        {
            Name = createClient.Name;
            Email = createClient.Email;
            BirthDate = createClient.BirthDate;
        }
    }
}
