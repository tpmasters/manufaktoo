﻿using Domain.Commands;
using System;

namespace Manufaktoo.Users.Contracts.Commands
{
    public class DeleteUserCommand : ICommand
    {
        public Guid Id { get; }

        public DeleteUserCommand(Guid id)
        {
            Id = id;
        }
    }
}
