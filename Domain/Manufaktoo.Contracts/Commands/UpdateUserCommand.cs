﻿using Domain.Commands;
using Manufaktoo.Users.Contracts.DTOs;
using System;

namespace Manufaktoo.Users.Contracts.Commands
{
    public class UpdateUserCommand : CreateUserCommand, ICommand
    {
        public Guid Id { get; }

        public UpdateUserCommand(Guid id, string name, string email, DateTime birthDate) 
            : base(name, email, birthDate)
        {
            Id = id;
        }
    }
}
