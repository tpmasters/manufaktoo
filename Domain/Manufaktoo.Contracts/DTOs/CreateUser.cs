﻿using System;

namespace Manufaktoo.Users.Contracts.DTOs
{
    public class CreateUser
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
