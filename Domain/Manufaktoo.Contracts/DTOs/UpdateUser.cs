﻿using Domain.Aggregates;
using System;

namespace Manufaktoo.Users.Contracts.DTOs
{
    public class UpdateUser : CreateUser, IAggregate
    {
        public Guid Id { get; set; }
    }
}
