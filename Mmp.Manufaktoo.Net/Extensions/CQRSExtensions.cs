﻿using Domain.Commands;
using Domain.Events;
using Domain.Queries;
using Manufaktoo.Users.Contracts.Commands;
using Manufaktoo.Users.Contracts.Queries;
using Manufaktoo.Users.Domain.Handlers;
using Manufaktoo.Users.Views;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace Mmp.Manufaktoo.Api.Extensions
{
    internal static class CQRSExtensions
    {
        internal static void ConfigureCQRS(this IServiceCollection services)
        {
            services.AddScoped<ICommandBus, CommandBus>();
            services.AddScoped<IQueryBus, QueryBus>();
            services.AddScoped<IEventBus, EventBus>();


            services.AddScoped<IAsyncRequestHandler<CreateUserCommand>, UsersCommandHandler>();
            services.AddScoped<IAsyncRequestHandler<UpdateUserCommand>, UsersCommandHandler>();
            services.AddScoped<IAsyncRequestHandler<DeleteUserCommand>, UsersCommandHandler>();
            services.AddScoped<IAsyncRequestHandler<GetUserList, List<UserListItem>>, UsersQueryHandler>();
            services.AddScoped<IAsyncRequestHandler<GetUser, UserItem>, UsersQueryHandler>();
        }
    }
}
