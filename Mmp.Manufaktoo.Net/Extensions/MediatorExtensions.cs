﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Mmp.Manufaktoo.Api.Extensions
{
    internal static class MediatorExtensions
    {
        internal static void ConfigureMediator(this IServiceCollection services)
        {
            services.AddMediatR(typeof(Startup));
            services.AddScoped<IMediator, Mediator>();
            services.AddTransient<SingleInstanceFactory>(sp => t => sp.GetService(t));
            services.AddTransient<MultiInstanceFactory>(sp => t => sp.GetServices(t));
        }
    }
}