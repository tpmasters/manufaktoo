﻿using Manufaktoo.Users.Storage;
using Manufaktoo.Users.Storage.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Mmp.Manufaktoo.Api.Extensions
{
    internal static class EFExtensions
    {
        internal static void ConfigureEF(this IServiceCollection services, IConfigurationRoot configurationRoot)
        {
            services.AddDbContext<UsersDbContext>(options => options.UseSqlServer(configurationRoot.GetConnectionString("ManufaktooDB")));
            services.AddIdentity<UserEntity, RoleEntity>()
                    .AddEntityFrameworkStores<UsersDbContext>()
                    .AddDefaultTokenProviders();
        }
    }
}
