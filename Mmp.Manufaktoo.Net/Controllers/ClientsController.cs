﻿using Domain.Commands;
using Domain.Queries;
using Manufaktoo.Users.Contracts.Commands;
using Manufaktoo.Users.Contracts.DTOs;
using Manufaktoo.Users.Contracts.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventSourcing.Sample.Web.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public UsersController(ICommandBus commandBus, IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }


        [HttpGet]
        public Task<List<UserListItem>> Get()
        {
            return _queryBus.Send<GetUserList, List<UserListItem>>(new GetUserList());
        }


        [HttpGet("{id}")]
        public Task<UserItem> Get(Guid id)
        {
            return _queryBus.Send<GetUser, UserItem>(new GetUser(id));
        }

        [HttpPost]
        public async Task Post([FromBody]CreateUser createUser)
        {
            var command = new CreateUserCommand(createUser);
            await _commandBus.Send(command);
        }

        [HttpPut]
        public async Task Put([FromBody]UpdateUser updateUser)
        {
            var command = new UpdateUserCommand(updateUser.Id, updateUser.Name,updateUser.Email, updateUser.BirthDate);
            await _commandBus.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task Post(Guid id)
        {
            await _commandBus.Send(new DeleteUserCommand(id));
        }
    }
}