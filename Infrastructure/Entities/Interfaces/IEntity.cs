﻿using System;

namespace Domain.Entities.Interfaces
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
